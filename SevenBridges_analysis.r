#!/usr/bin/env Rscript

library(tidyverse);
library(Seurat);

list.files("SevenBridges");

## https://scomix.bd.com/hc/en-us/articles/360044971032-Bioinformatics
## Note: RSEC/DBEC - They are UMI adjustment algorithms to remove the
## effect of UMI errors on molecule counting. MI errors that are
## single-base substitution errors are identified and adjusted to the
## parent MI barcode using RSEC. Other MI errors derived from library
## preparation steps or sequencing base deletions are later adjusted
## using distribution-based error correction (DBEC). Refer to the BD
## Single-Cell Multiomics Bioinformatics Handbook for more
## information.
## https://scomix.bd.com/hc/article_attachments/4423045606285/23-21713_02__scM_Bioinformatics_Handbook-ruo.pdf

libraries.seurat <-
    sapply(c(S1 = "SevenBridges/31-10-22-Library1_Expression_Data.st",
             S2 = "SevenBridges/31-10-22-Library2_Expression_Data.st",
             S3 = "SevenBridges/31-10-22-Library3_Expression_Data.st"),
          function(fName){
              read_tsv(fName,comment="#", show_col_types=FALSE) %>%
                  select(Cell_Index, Bioproduct, RSEC_Adjusted_Molecules) %>%
                  pivot_wider(names_from=Cell_Index,
                              values_from=RSEC_Adjusted_Molecules,
                              id_cols=Bioproduct) %>%
                  column_to_rownames("Bioproduct") %>%
                                  as.matrix() %>%
                  CreateSeuratObject(project="PEA11303");
          });

pbmc <- merge(libraries.seurat[[1]],
              c(libraries.seurat[[2]], libraries.seurat[[3]]),
              add.cell.ids <- names(libraries.seurat), project="PEA11303");

## remove pre-merged object to save a bit of memory
rm(libraries.seurat);

## Work out cell count summaries (because the merge process destroys this information)
## [it's possible this could have been fetched from libraries.seurat]
map_dfr(c(S1 = "SevenBridges/31-10-22-Library1_Expression_Data.st",
          S2 = "SevenBridges/31-10-22-Library2_Expression_Data.st",
          S3 = "SevenBridges/31-10-22-Library3_Expression_Data.st"),
        function(fName){
            read_tsv(fName,comment="#", show_col_types=FALSE) %>%
                group_by(Cell_Index) %>%
                summarise(totalCount = sum(RSEC_Adjusted_Molecules),
                          totalFeatures = n());
        } , .id="library") %>%
    mutate(Cell_Index=paste0(library, "_", Cell_Index)) %>%
    select(-library) -> cell.count.summaries.tbl;

## Work out gene count summaries (because the merge process destroys this information)
## [it's possible this could have been fetched from libraries.seurat]
map_dfr(c(S1 = "SevenBridges/31-10-22-Library1_Expression_Data.st",
          S2 = "SevenBridges/31-10-22-Library2_Expression_Data.st",
          S3 = "SevenBridges/31-10-22-Library3_Expression_Data.st"),
        function(fName){
            read_tsv(fName,comment="#", show_col_types=FALSE) %>%
                group_by(Bioproduct) %>%
                summarise(totalCount = sum(RSEC_Adjusted_Molecules),
                          maxCount = max(RSEC_Adjusted_Molecules),
                          totalCells = n());
        } , .id="library") -> gene.count.summaries.tbl;

gene.count.summaries.tbl %>%
    group_by(Bioproduct) %>%
    summarise(totalCount = sum(totalCount)) %>%
    pull(totalCount) %>%
    pmin(100) %>%
    table()


gene.count.summaries.tbl %>%
    group_by(Bioproduct) %>%
    summarise(totalCells = sum(totalCount)) %>%
    pull(totalCells) %>%
    pmin(100) %>%
    table()

## The noise floor is chosen based on an assumption of 0.2% barcode crossover
gene.count.summaries.tbl %>%
    pull(maxCount) %>%
    max() * 0.002 -> noiseFloor


options(width = 180)

gene.count.summaries.tbl %>%
    group_by(Bioproduct) %>%
    summarise(totalCount = sum(totalCount), totalCells = sum(totalCells)) %>%
    filter(totalCount <= (noiseFloor * 2)) %>%
    pull(Bioproduct) -> lowCountGenes

## Sanity check - make sure columns match
all(colnames(pbmc) %in% cell.count.summaries.tbl$Cell_Index);
all(cell.count.summaries.tbl$Cell_Index %in% colnames(pbmc));

sb.fileNames <- sort(list.files("SevenBridges", pattern="Sample_Tag", full.names=TRUE));
names(sb.fileNames) <- paste0("S",sub("^.*Library(.).*$","\\1",sb.fileNames));

map_dfr(sb.fileNames, function(fName){
    read_csv(fName, comment="#", show_col_types=FALSE)
}, .id="run") %>%
    pivot_longer(cols=starts_with("SampleTag"),
                 names_to="tag", values_to="count") %>%
    filter(count > 0) %>%
    mutate(Cell_Index=paste0(run, "_", Cell_Index),
           tag = sub("_.*$", "", tag)) %>%
    select(-run)  %>%
    arrange(tag) %>%
    pivot_wider(names_from=Cell_Index,
                values_from=count,
                id_cols=tag) %>%
    column_to_rownames("tag") %>%
    as.matrix() -> cellTags.data.mat;

cellTags.mat <- matrix(NA, nrow=12, ncol=ncol(pbmc));
colnames(cellTags.mat) <- colnames(pbmc);
rownames(cellTags.mat) <- rownames(cellTags.data.mat);
cellTags.mat[,colnames(cellTags.data.mat)] <- cellTags.data.mat;

cellTags.assay <- CreateAssayObject(cellTags.mat);
## double-check to make sure merge is correct
all(colnames(cellTags.assay) == colnames(pbmc));
## Add cell tag information to Seurat dataset
pbmc[["CellTags"]] <- cellTags.assay;

pbmc$noCellTags <- apply(pbmc$CellTags[], 2, function(x){all(is.na(x))});
pbmc$totalTagCount <- apply(pbmc$CellTags[], 2, function(x){if(all(is.na(x))) { 0 } else { sum(x, na.rm=TRUE) }});
pbmc$maxTagCount <- apply(pbmc$CellTags[], 2, function(x){if(all(is.na(x))) { 0 } else { max(x, na.rm=TRUE) }});
pbmc$minTagCount <- apply(pbmc$CellTags[], 2, function(x){if(all(is.na(x))) { 0 } else { min(x, na.rm=TRUE) }});
pbmc$minTagCount[pbmc$maxTagCount == pbmc$totalTagCount] <- 0; # set minimum to zero if only one count
pbmc$maxTagRatio <- ifelse(pbmc$totalTagCount == 0, 0, pbmc$maxTagCount / pbmc$totalTagCount);
pbmc$nCount_RNA <-
    (cell.count.summaries.tbl %>%
     pull(totalCount, name=Cell_Index))[colnames(pbmc)];
pbmc$nFeature_RNA <-
    (cell.count.summaries.tbl %>%
     pull(totalFeatures, name=Cell_Index))[colnames(pbmc)];

## Make sure RNAs have counts
range(pbmc$nCount_RNA, na.rm=TRUE);

## Work out reasonable thresholds for total tag count (>16)
rowSums(table(pmin(pbmc$totalTagCount, 40), round(pbmc$maxTagRatio, 1)));

table(pbmc$totalTagCount > 16, pbmc$maxTagRatio > 0.75);

## Work out reasonable thresholds for minimum tag count (<20)
table(pmin(pbmc$minTagCount[pbmc$totalTagCount > 16], 40),
      round(pbmc$maxTagRatio[pbmc$totalTagCount > 16], 1));

rowSums(table(pmin(pbmc$minTagCount[pbmc$totalTagCount > 16], 40),
              round(pbmc$maxTagRatio[pbmc$totalTagCount > 16], 1)));

## Work out reasonable threshold for max tag ratio (>= 0.75%)
table(round(pbmc$maxTagRatio[pbmc$totalTagCount > 16 & pbmc$minTagCount < 20], 2));

## Look at summary table
table(A=pbmc$totalTagCount > 16, B=pbmc$minTagCount < 20, C=pbmc$maxTagRatio >= 0.75);

## Check minimum tag count vs total RNA [not sure what this would be useful for]
table(round(pbmc$minTagCount / pbmc$nCount_RNA, 3));

## look at RNA / mt content
pbmc[["percent.mt"]] <- PercentageFeatureSet(pbmc, pattern = "^MT-");
pbmc[["goodTags"]] <- (pbmc$totalTagCount > 16) & (pbmc$minTagCount < 20) & (pbmc$maxTagRatio >= 0.75);

## plot out transcript / read count statistics
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3,
        split.by="goodTags");
ggsave("violinPlot_feature_count_mt.png", width=16, height=8);

## check RNA feature / count filtering
## [looks like SevenBridges + tag selection has already done enough filtering for this]
png("density_RNAfeatures.png", width=1280, height=720);
plot(density(log2(pbmc$nFeature_RNA[pbmc$goodTags])));
abline(v=log2(500));
dev.off();
png("density_RNAcounts.png", width=1280, height=720);
plot(density(log2(pbmc$nCount_RNA[pbmc$goodTags])));
abline(v=log2(1000));
dev.off();

pbmc.filtered <-
    subset(pbmc,(totalTagCount > 16) & (minTagCount < 20) & (maxTagRatio >= 0.75),
           features = setdiff(rownames(pbmc), lowCountGenes));

## Write unfiltered dataset to disk
saveRDS(pbmc, "OG_PBMC_unfiltered_2022-Nov-05.rds");
## check column count is roughly correct
ncol(pbmc.filtered);
## ... then remove unfiltered dataset
rm(pbmc);


## re-add Sample Tags

sb.fileNames <- sort(list.files("SevenBridges", pattern="Sample_Tag", full.names=TRUE));
names(sb.fileNames) <- paste0("S",sub("^.*Library(.).*$","\\1",sb.fileNames));

map_dfr(sb.fileNames, function(fName){
    read_csv(fName, comment="#", show_col_types=FALSE)
}, .id="run") %>%
    pivot_longer(cols=starts_with("SampleTag"),
                 names_to="tag", values_to="count") %>%
    filter(count > 0) %>%
    mutate(Cell_Index=paste0(run, "_", Cell_Index),
           tag = sub("_.*$", "", tag)) %>%
    select(-run)  %>%
    arrange(tag) %>%
    filter(Cell_Index %in% colnames(pbmc.filtered)) %>%
    pivot_wider(names_from=Cell_Index,
                values_from=count,
                id_cols=tag) %>%
    column_to_rownames("tag") %>%
    as.matrix() -> cellTags.data.mat;

cellTags.mat <- matrix(NA, nrow=12, ncol=ncol(pbmc.filtered));
colnames(cellTags.mat) <- colnames(pbmc.filtered);
rownames(cellTags.mat) <- rownames(cellTags.data.mat);
cellTags.mat[,colnames(cellTags.data.mat)] <- cellTags.data.mat;

cellTags.assay <- CreateAssayObject(cellTags.mat);
## double-check to make sure merge is correct
all(colnames(cellTags.assay) == colnames(pbmc.filtered));
## Add cell tag information to Seurat dataset
pbmc.filtered[["CellTags"]] <- cellTags.assay;

sampleTagMapping <-
    c("SampleTag01" = "006_0h", "SampleTag02" = "006_6h", "SampleTag03" = "006_24h",
      "SampleTag04" = "016_0h", "SampleTag05" = "016_6h", "SampleTag06" = "016_24h",
      "SampleTag07" = "022_0h", "SampleTag08" = "022_6h", "SampleTag09" = "022_24h",
      "SampleTag10" = "023_0h", "SampleTag11" = "023_6h", "SampleTag12" = "023_24h");



## Write filtered dataset to disk
saveRDS(pbmc.filtered, "OG_PBMC_filtered_2022-Nov-05.rds");





rownames(pbmc$CellTags);


## Note: AHR Gene Set
## https://maayanlab.cloud/Harmonizome/gene_set/AHR/CHEA+Transcription+Factor+Targets
