#!/usr/bin/env Rscript
library(SingleCellExperiment);
library(Seurat);
## remotes::install_github('satijalab/seurat-wrappers');
library(SeuratWrappers);
library(tidyverse);

tagCountFiles <- c(
    "S1" = "cellTagCounts_S1.txt.gz",
    "S2" = "cellTagCounts_S2.txt.gz",
    "S3" = "cellTagCounts_S3.txt.gz");

map_dfr(tagCountFiles, function(fName){
    read_table(fName, col_names = c("count", "cell", "tag")) %>%
        mutate(cell = sub("cell=", "", cell),
               tag = sub("mapping=", "", tag));
    }, .id="run") -> cellTagCounts.tbl;

cellTagCounts.tbl %>%
    group_by(cell, tag) %>%
    filter(n() > 1, max(count) >= 10) %>%
    ungroup() %>%
    arrange(-count, cell) %>%
    pivot_wider(names_from="run", values_from="count") -> cellTagCounts.wide.tbl

cellTagCounts.tbl %>%
    group_by(cell, tag) %>%
    filter(n() > 1, max(count) >= 10) %>%
    summarise(maxCount = max(count), minCount = min(count)) %>%
    arrange(-maxCount, -minCount) -> cellTagCounts.summary.tbl

cellTagCounts.summary.tbl %>%
    ggplot() +
    aes(x = log10(maxCount), y=log10(minCount), col=tag) +
    geom_point()
ggsave("out.png", width=11, height=8);


cellTagCounts.tbl %>%
    group_by(cell) %>%
    summarise(maxtagCount = max(count)) %>%
    filter(maxtagCount > 5) %>%
    pull(maxtagCount) %>% median()

cellTagCounts.tbl %>%
    group_by(cell) %>%
    summarise(maxTagCount = max(count)) %>%
    filter(maxTagCount > 1)


cellTagCounts.tbl %>%
    group_by(cell, run) %>%
    summarise(totalCount = sum(count), maxTagCount = max(count)) %>%
    filter(maxTagCount > 1) %>%
    group_by(cell) %>%
    summarise(totalCount = sum(totalCount)) %>%
    arrange(totalCount) %>%
    pull(cell) %>%
    cat(file=sprintf("goodSampleTagCells_%s.txt", Sys.Date()), sep="\n");

head(cellTagCounts.wide.tbl);
tail(cellTagCounts.wide.tbl);

pbmc <- ReadAlevin("salmon_1.9_cbc_whitelist_OG_2022-Oct-13_combined/alevin/quants_mat.gz");

head(colnames(pbmc));

ncol(pbmc);
sum(colnames(pbmc) %in% cellTagCounts.tbl$cell);

tag.mat <- (pbmc[["RNA"]])[grepl("SampleTag", rownames(pbmc)),];

str(tag.mat);

rowSums(tag.mat);


read_table("cellBarcodeInfo.txt.gz",
           col_names=c("count","sample","lane","barcode")) %>%
    filter(barcode %in% colnames(pbmc)) -> cellCounts.tbl;

tag.mat[,"GTGTCCTTAGCCTGTTCAAGCCTGGTT"]

## [check to make sure that barcode/sample/lane triplets are unique
cellCounts.tbl %>%
    group_by(barcode, sample, lane) %>%
    summarise(n = n(), .groups = "drop") %>%
    filter(n > 1)

cellCounts.tbl %>%
    arrange(-count) %>%
    pivot_wider(names_from=c(sample, lane), values_from=count) -> cellCounts.wide.tbl

options(width=130);
cellCounts.wide.tbl;

## Identify library doublets (where the same cell barcode was present in separate libraries)
cellCounts.tbl %>%
    group_by(barcode, sample) %>%
    summarise(count = sum(count), .groups="drop") %>%
    group_by(barcode) %>%
    mutate(maxCount = max(count), sumCount = sum(count), nCount = n(),
           maxLib = ifelse(count == maxCount, sample, ""),
           maxRatio = maxCount / sumCount) %>%
    filter(maxRatio < 0.75) %>%
    arrange(-maxCount, barcode) %>%
    select(cell=barcode, sample, count) %>%
    pivot_wider(names_from=sample, values_from=count) -> cellLibraryDoublets.tbl

write_csv(cellLibraryDoublets.tbl, "cellLibraryDoublets.csv");

cellTagCounts.tbl %>%
    filter(cell %in% colnames(pbmc)) %>%
    mutate(tag = sub("^NewSampleTag", "", tag)) %>%
    group_by(cell, tag) %>% # can combined different libraries now, as have excluded library doublets
    summarise(count = sum(count), .groups="drop") %>%
    group_by(cell) %>%
    mutate(maxCount = max(count), sumCount = sum(count), nCount = n(),
           maxTag = ifelse(count == maxCount, tag, ""),
           maxRatio = maxCount / sumCount) %>%
    filter(maxRatio < 0.75) %>%
    arrange(-maxCount, cell) %>%
    select(cell, tag, count) %>%
    pivot_wider(names_from=tag, values_from=count) -> sampleTagDoublets.tbl

write_csv(sampleTagDoublets.tbl, "sampleTagDoublets.csv");

cellTagCounts.tbl %>%
    filter(cell %in% sampleTagDoublets.tbl$cell) %>%
    mutate(tag = factor(sub("^NewSampleTag", "", tag))) %>%
    select(cell, run, tag, count) %>%
    arrange(tag, cell, run) %>%
    pivot_wider(names_from=c(tag), values_from=count) %>%
    mutate(tagSumLib=rowSums(across(where(is.numeric)), na.rm=TRUE)) %>%
    group_by(cell) %>%
    mutate(tagSumAll = sum(tagSumLib)) %>%
    arrange(-tagSumAll, cell, run) -> sampleTagDoublets.lib.tbl

write_csv(sampleTagDoublets.lib.tbl, "sampleTagDoubletsLib.csv");

## Remove library & tag doublets from dataset
table(colnames(pbmc) %in% sampleTagDoublets.tbl$cell);
table(colnames(pbmc) %in% cellLibraryDoublets.tbl$cell);

table(colnames(pbmc) %in% sampleTagDoublets.tbl$cell) / ncol(pbmc);
table(colnames(pbmc) %in% cellLibraryDoublets.tbl$cell) / ncol(pbmc);
      
table(colnames(pbmc) %in% sampleTagDoublets.tbl$cell,
      colnames(pbmc) %in% cellLibraryDoublets.tbl$cell)

doubletCells <-
    (colnames(pbmc) %in% cellLibraryDoublets.tbl$cell |
     colnames(pbmc) %in% sampleTagDoublets.tbl$cell)

## plot out transcript / read count statistics
pbmc[["percent.mt"]] <- PercentageFeatureSet(pbmc, pattern = "^MT-");
pbmc[["likelyDoublet"]] <- doubletCells;
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3,
        split.by="likelyDoublet");
ggsave("violinPlot_feature_count_mt.png", width=16, height=8);

png("density_RNAfeatures.png", width=1280, height=720);
plot(density(log2(pbmc$nFeature_RNA[!doubletCells])));
abline(v=log2(500));
dev.off();

png("density_RNAcounts.png", width=1280, height=720);
plot(density(log2(pbmc$nCount_RNA[!doubletCells])));
abline(v=log2(1000));
dev.off();

## filter out doublets, and [very] low read counts
pbmc.filtered <- subset(pbmc, cells = which(!doubletCells), nFeature_RNA > 500 & nCount_RNA > 1000);

## filter out cells with fewer than 100 reads
sum(colSums(pbmc.filtered$RNA@counts) > 100)

## Collect up Sequencing Library IDs
cellCounts.tbl %>%
    filter(barcode %in% colnames(pbmc.filtered)) %>%
    group_by(barcode, sample) %>%
    summarise(count = sum(count), .groups="drop") %>%
    group_by(barcode) %>%
    mutate(maxCount = max(count), sumCount = sum(count), nCount = n(),
           maxLib = ifelse(count == maxCount, sample, ""),
           maxRatio = maxCount / sumCount) %>%
    summarise(maxLib = paste(maxLib, collapse="")) %>%
    pull(maxLib, name=barcode) -> libraryIDs

## Collect up Sample Tag IDs
cellTagCounts.tbl %>%
    filter(cell %in% colnames(pbmc.filtered)) %>%
    mutate(tag = sub("^HumanSampleTag", "X", tag)) %>%
    group_by(cell, tag) %>%
    summarise(count = sum(count), .groups="drop") %>%
    group_by(cell) %>%
    mutate(maxCount = max(count), sumCount = sum(count), nCount = n(),
           maxTag = ifelse(count == maxCount, tag, ""),
           maxRatio = maxCount / sumCount) %>%
    summarise(maxTag = paste(maxTag, collapse="")) %>%
    pull(maxTag, name=cell) -> sampleTagIDs

## Add Sample Tag to Seurat Object metadata
pbmc.filtered$library <- libraryIDs[colnames(pbmc.filtered)];
pbmc.filtered$sampleTag <- sampleTagIDs[colnames(pbmc.filtered)];

options(width=130);

table(pbmc.filtered$library);
table(pbmc.filtered$sampleTag);

median(colSums(pbmc.filtered$RNA))

table(colSums(pbmc.filtered$RNA) < 100)

## Collect cell cycle genes (updated 2019 list)
## [https://satijalab.org/seurat/articles/cell_cycle_vignette.html]
s.genes <- cc.genes.updated.2019$s.genes
g2m.genes <- cc.genes.updated.2019$g2m.genes

s.genes <- intersect(s.genes, rownames(pbmc.filtered));
g2m.genes <- intersect(g2m.genes, rownames(pbmc.filtered));

## Correct for cell cycle
pbmc.filtered <-
    CellCycleScoring(pbmc.filtered, s.features = s.genes,
                     g2m.features = g2m.genes, set.ident = TRUE);

## Check cell phase (ideally, no cells unclassified)
table(pbmc.filtered$Phase);

## Run rPCA integration [initially without cell-cycle correction]
## [https://satijalab.org/seurat/articles/integration_rpca.html]
pbmc.list <- SplitObject(pbmc.filtered, split.by = "sampleTag");

## normalize and identify variable features for each dataset independently
pbmc.list <- lapply(X = pbmc.list, FUN = function(x) {
    x <- NormalizeData(x)
    x <- FindVariableFeatures(x, selection.method = "vst", nfeatures = 2200)
});

## select features that are repeatedly variable across datasets for integration run PCA on each
## dataset using these features
sampleTagList <- grep("SampleTag", rownames(pbmc.filtered), value=TRUE); # should be excluded from features
features <- setdiff(SelectIntegrationFeatures(object.list = pbmc.list), sampleTagList);

## collect up invariant feature list for data subsets
invariantFeatures <- NULL;
for( x in pbmc.list){
    featureStDev <- apply(x$RNA@counts[features,], 1, sd);
    if(any(featureStDev == 0)){
        invariantFeatures <- c(invariantFeatures, names(featureStDev)[featureStDev == 0]);
    }
    invisible(NULL);
}
## Exclude invariant features from feature list
features <- setdiff(features, invariantFeatures);

pbmc.list <- lapply(X = pbmc.list, FUN = function(x) {
    ## Regress on cell cycle scores and library to reduce cycle influence on PCA
    x <- ScaleData(x, vars.to.regress = c("S.Score", "G2M.Score", "library"),
                   features = features, verbose = TRUE);
    x <- RunPCA(x, features = features, verbose = TRUE);
    });

pbmc.anchors <- FindIntegrationAnchors(object.list = pbmc.list, anchor.features = features, reduction = "rpca");

## this command creates an 'integrated' data assay
pbmc.combined <- IntegrateData(anchorset = pbmc.anchors);

## specify that we will perform downstream analysis on the corrected data note that the
## original unmodified data still resides in the 'RNA' assay
DefaultAssay(pbmc.combined) <- "integrated";

## Run the standard workflow for visualization and clustering (with cell cycle regression)
## [I'm not sure whether or not I should do the regression again here] 
pbmc.combined <- ScaleData(pbmc.combined, vars.to.regress = c("S.Score", "G2M.Score", "library"),
                           verbose = TRUE);
pbmc.combined <- RunPCA(pbmc.combined, npcs = 30, verbose = TRUE);
## [Note: UMAP with 200 epochs default didn't seem like it was collapsed enough]
pbmc.combined <- RunUMAP(pbmc.combined, reduction = "pca", dims = 1:30, n.epochs=200);
pbmc.combined <- FindNeighbors(pbmc.combined, reduction = "pca", dims = 1:30);
pbmc.combined <- FindClusters(pbmc.combined, resolution = 0.5);

pbmc.combined$cluster <- Idents(pbmc.combined);

library(viridis);

DimPlot(pbmc.combined, reduction = "umap", group.by = "library",
        label = TRUE, repel = TRUE) +
    scale_colour_viridis_d(begin=0.1, option="H");
ggsave(sprintf("UMAP_ST_library_%s.png", Sys.Date()), width=14, height=10);

DimPlot(pbmc.combined, reduction = "umap", group.by = "sampleTag",
        label = TRUE, repel = TRUE) +
    scale_colour_viridis_d(begin=0.1, option="H");
ggsave(sprintf("UMAP_ST_sampleTag_%s.png", Sys.Date()), width=14, height=10);

DimPlot(pbmc.combined, reduction = "umap", group.by = "cluster",
        label = TRUE, repel = TRUE)  +
    scale_colour_viridis_d(begin=0.1, option="H");
ggsave(sprintf("UMAP_ST_cluster_%s.png", Sys.Date()), width=14, height=10);

DimPlot(pbmc.combined, reduction = "umap", group.by = "Phase",
        label = TRUE, repel = TRUE) +
    scale_colour_viridis_d(begin=0.1, option="H");
ggsave(sprintf("UMAP_ST_phase_%s.png", Sys.Date()), width=14, height=10);

table(pbmc.combined$cluster, pbmc.combined$library);

saveRDS(pbmc.combined, "PBMC_combined_2022-Oct-28.rds");


### SevenBridges Comparison
source("Rhapsody_cell_keys.r");

sb.fileNames <- list.files("SevenBridges", pattern="Sample_Tag", full.names=TRUE);
names(sb.fileNames) <- paste0("S",sub("^.*Library(.).*$","\\1",sb.fileNames));

map_dfr(sb.fileNames, function(fName){
    read_csv(fName, comment="#", show_col_types=FALSE)
}, .id="run") %>%
    mutate(cell=rhapIndexToSeq(Cell_Index)) %>%
    pivot_longer(cols=starts_with("SampleTag"),
                 names_to="tag", values_to="count") %>%
    filter(count > 0) %>%
    mutate(tag = sub("_.*$", "", sub("^", "Human", tag))) %>%
    mutate(platform="SevenBridges") -> sb.cellTagCounts.tbl;    
    
tail(sb.cellTagCounts.tbl)

cellTagCounts.tbl %>%
    mutate(platform="Alevin") %>%
    bind_rows(sb.cellTagCounts.tbl) %>% tail()

seqToRhapIndex("NACGCTAAGCAACTCCAGGG")
