Salmon Rhapsody
===

Accessory scripts and files for processing Rhapsody data using Salmon.

The workflow outlined in 'process.txt' also references scripts from my [bioinfscripts](https://gitlab.com/gringer/bioinfscripts) repository:

 * synthSquish.pl -- for correcting cell barcodes, and merging them into a format that is easier for `salmon alevin` to understand
 * copyMapping.pl -- for combining sample tag identification with cell barcodes
